import { StyleSheet } from "react-native"

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#2c3e50",
  },
  renderFlatList: {
    marginBottom: 10, 
    paddingLeft: 20,
    padding: 2,
    borderRadius: 15,
    backgroundColor: "#1787cf",
  },
  title: {
    fontWeight: 'bold',
    color: '#fff',
    fontSize: 20
  },
  info: {
    color: '#fff',
    fontSize: 18,
    marginTop: 4
  },
  icon: {
    height: 30,
    width: 30
  }
})

export default styles;