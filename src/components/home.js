import React, { useState } from "react"
import { Text, View, TouchableHighlight, Alert, StyleSheet } from "react-native"

export default function Test({ navigation }) {
  const [users, setUsers] = useState(0)
  const [products, setProducts] = useState(0)
  const [sales, setSales] = useState(0)
  const [totalSales, setTotalSales] = useState(0)

  return (
      <View style={styles.container}>
        <TouchableHighlight 
          underlayColor="#8ed1fc" 
          onPress={() => navigation.push("Client")}>
          <View style={styles.viewText}>
            <Text style={styles.text}> Usuários cadastrados</Text>
          </View>
        </TouchableHighlight>

        <TouchableHighlight 
          underlayColor="#8ed1fc" 
          onPress={() => navigation.push("Stoke")}>
          <Text style={styles.text}> Produtos no estoque</Text>
        </TouchableHighlight>

        <TouchableHighlight 
          underlayColor="#8ed1fc" 
          onPress={() => navigation.push("Stoke")}>
          <Text style={styles.text}> Total de vendas</Text>
        </TouchableHighlight>
      </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#2c3e50"
  },
  text: {
    fontSize: 24,
    padding: 21,
    paddingLeft: 10,
    borderRadius: 13,
    fontWeight: 'bold',
    marginTop: 6,
    borderBottomWidth: 2,
    borderBottomColor: "#2c3e50",
    color: '#d9e3f0',
    backgroundColor: "#1787cf"
  }
})