import React from "react"
import { StatusBar, Image } from "react-native"
import { NavigationContainer } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


import Home from './home'
import Client from './listClients.js'
import Stoke from './listStock'
import RegisterSale from "./registerSale"
import RegisterClient from "./registerClient"
import RegisterProducts from "./registerProducts";
import styles from "../styles/pagesInfo"

const HomeStack = createStackNavigator();
const Tab = createBottomTabNavigator();

const HomeStackScreen = () => {
  return (
    <HomeStack.Navigator >
      <HomeStack.Screen name='Home' component={Home} options={{title:"Menu"}} />
      <HomeStack.Screen name='Stoke' component={Stoke} options={{title:"Estoque"}}/>
      <HomeStack.Screen name='Client' component={Client} options={{title:"Clientes"}}/>
    </HomeStack.Navigator>
  )
}

export default function Test() {
  return (
   <NavigationContainer>
    <StatusBar hidden={true} />
     <Tab.Navigator>
       <Tab.Screen 
         name='Home' 
         component={HomeStackScreen} 
         options={{ 
           title: "Menu",
           tabBarIcon: ({ color, size }) => (
            <Image 
              style={styles.icon}
              source={require("../assets/iconHouse.png")}/>
          ),
           }} />
       <Tab.Screen 
         name='RegisterClient' 
         component={RegisterClient} 
         options={{ 
           title: "Clientes",
           tabBarIcon: ({ color, size }) => (
            <Image 
              style={styles.icon}
              source={require("../assets/iconClient.png")}/>
          ),
           }} />
       <Tab.Screen 
         name='RegisterProducts' 
         component={RegisterProducts} 
         options={{ 
           title: "Produtos", 
           tabBarIcon: ({ color, size }) => (
            <Image 
              style={styles.icon}
              source={require("../assets/iconProducts.png")}/>
          ),
           }} />
       <Tab.Screen 
         name='RegisterSale' 
         component={RegisterSale} 
         options={{ 
          title: "Vendas", 
          tabBarIcon: ({ color, size }) => (
            <Image 
              style={styles.icon}
              source={require("../assets/iconSales.png")}/>
          ),
          }} />
     </Tab.Navigator>
   </NavigationContainer>
  )
}
